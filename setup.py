from setuptools import setup
import naclautomake

setup(name = 'naclautomake',
      version = naclautomake.__version__,
      description = 'Make it easier to write Makefile of Native Client project.',
      license = 'Creative Commons Attribution 3.0 Unported License',
      author = 'Alex Chi',
      author_email = 'alex@alexchi.me',
      url = 'http://repo.alexchi.me/nacl-automake',
      packages = ['naclautomake'],
      keywords = 'automake makefile native client nacl google',
      classifiers = [
                     "Programming Language :: Python",
                     "Programming Language :: Python :: 2.7",
                     "Development Status :: 1 - Planning",
                     "License :: OSI Approved",
                     "Operating System :: OS Independent",
                     "Topic :: Software Development",
                     "Topic :: Utilities",
                    ],
      long_description = 'NaClAutomake can help you to create Makefile of Native Client project by simple configuration.')
