'''
Created on 2013-2-7

@author: Alex Chi
'''
from common import EProjectType
from makefile import NaClMakefile
from project import NaClProject

class NaClSolution(NaClMakefile):
    '''
a solution that contain all projects
    '''

    def __init__(self, name, setting):
        self.__name__ = name
        self.__setting__ = setting
        self.__projects__ = {}

    @property
    def name(self):
        return self.__name__

    def newProject(self, name, projectpath, projecttype = EProjectType.EXE):
        self.__projects__[name] = NaClProject(name, self.__setting__, projectpath, projecttype, self)
        return self.__projects__[name]

    def depend(self, dependname, bedependname):
        if dependname not in self.__projects__ and bedependname not in self.__projects__:
            raise Exception('can\'t find the ' + dependname + ' or ' + bedependname + ' from projects')
        # TODO: need to check the relationship between dependname and bedependname
        self.__projects__[dependname].addDepend([bedependname])
        self.__projects__[bedependname].addBedepend([dependname])

    def mfSolution(self):
        res = '# Solution: ' + self.__name__
        res = '\n'.join([res, self.__name__ + ':'])
        for key in self.__projects__:
            if len(self.__projects__[key].bedepends) <= 0:
                res = ' '.join([res, key])
        return res

    def mfProjects(self):
        res = '# The content of all projects'
        for key in self.__projects__:
            res = '\n'.join([res, ''])
            res = '\n'.join([res, self.__projects__[key].makefile()])
        return res

    def makefile(self):
        res = self.mfSolution()
        res = '\n'.join([res, ''])
        res = '\n'.join([res, self.mfProjects()])
        return res