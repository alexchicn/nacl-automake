'''
Created on 2013-3-17

@author: Alex
'''
from makefile import NaClMakefile

from common import ECodeFileExt
from common import ECodeFileType
from common import ECodeType

class NaClCompiler(NaClMakefile):
    '''
a compiler for native client
    '''
    def __init__(self, outputpath, toolchain, maketype, ostypes, projecttype):
        '''
the compiler must known what kind of toolchain
        '''
        self.__outputpath__ = outputpath
        self.__toolchain__ = toolchain
        self.__maketype__ = maketype
        self.__ostypes__ = ostypes
        self.__projecttype__ = projecttype

    def getAllIncludePath(self, project):
        res = ''
        if 0 < len(project.incPaths):
            res = '-I'
            res += ' -I'.join(project.incPaths)
        return res

    def mfKey(self, outputpath, codefile):
        res = 'key'
        return res

    def mfDependency(self, outputpath, codefile):
        res = 'dependency'
        return res

    def mfAllCodeFile(self, project):
        res = '# compile code'
        for codefile in project.srcs:
            res = '\n'.join([res, ': '.join([self.mfKey(project.outputpath, codefile), self.mfDependency(project.outputpath, codefile)])])
        return res

    def makefile(self, project):
        res = self.getAllIncludePath(project)
        res = '\n'.join([res, self.mfAllCodeFile(project)])
        return res