'''
Created on 2013-2-9

@author: Alex
'''
from common import EToolChain
from common import EMakeType
from setting import NaClSetting
from makefile import NaClMakefile

class NaClDefine(NaClMakefile):
    '''
all defines
    '''
    def __init__(self, setting):
        # check the type of input
        if type(setting) != NaClSetting:
            raise Exception('Sorry, the argument must be NaClSetting instance!')
        self.__setting__ = setting

    @property
    def setting(self):
        return self.__setting__

    def mfDefine(self):
        res = '# Var defines'
        res = '\n'.join([res, 'OSNAME:=$(shell python $(NACL_SDK_ROOT)/tools/getos.py)'])
        res = '\n'.join([res, 'TC_PATH:=$(abspath $(NACL_SDK_ROOT)/toolchain)'])
        return res

    def mfPath(self):
        res = '# Path defines'
        res = '\n'.join([res, 'THIS_MAKEFILE:=$(abspath $(lastword $(MAKEFILE_LIST)))'])
        res = '\n'.join([res, 'THIS_MAKEFILE_PATH:=$(abspath $(dir $(THIS_MAKEFILE)))'])
        res = '\n'.join([res, 'SOLUTION_PATH?=' + self.__setting__.solutionPath])
        res = '\n'.join([res, 'OUTPUT_PATH?=' + self.__setting__.outputPath])
        res = '\n'.join([res, 'CHROME_PATH?=' + self.__setting__.chromePath])
        res = '\n'.join([res, 'NACL_SDK_ROOT?=' + self.__setting__.naclsdkPath])
        res = '\n'.join([res, 'NACL_TOOLCHAIN_PATH:=$(abspath $(NACL_SDK_ROOT)/toolchain)'])
        return res

    def mfCommand(self):
        res = '# Command defines'
        res = '\n'.join([res, 'CREATENMF:=python $(NACL_SDK_ROOT)/tools/create_nmf.py'])
        res = '\n'.join([res, 'OSCOMMAND:=python $(NACL_SDK_ROOT)/tools/oshelpers.py'])
        res = '\n'.join([res, 'CP:=$(OSCOMMAND) cp'])
        res = '\n'.join([res, 'MKDIR:=$(OSCOMMAND) mkdir'])
        res = '\n'.join([res, 'MV:=$(OSCOMMAND) mv'])
        res = '\n'.join([res, 'RM:=$(OSCOMMAND) rm'])
        return res

    def mfDefault(self):
        res = '# Default defines'
        res = '\n'.join([res, 'NACL_WARNINGS:=-Wno-long-long -Wall -Wswitch-enum -Werror -pedantic'])
        return res

    def mfCompilerNewLib(self):
        res = '# NEWLIB compiler'
        res = '\n'.join([res, 'NEWLIB_CC?=$(TC_PATH)/$(OSNAME)_x86_newlib/bin/i686-nacl-gcc -c'])
        res = '\n'.join([res, 'NEWLIB_CXX?=$(TC_PATH)/$(OSNAME)_x86_newlib/bin/i686-nacl-g++ -c'])
        res = '\n'.join([res, 'NEWLIB_LINK?=$(TC_PATH)/$(OSNAME)_x86_newlib/bin/i686-nacl-g++ -Wl,-as-needed'])
        res = '\n'.join([res, 'NEWLIB_LINK?=$(TC_PATH)/$(OSNAME)_x86_newlib/bin/i686-nacl-g++ -Wl,-as-needed'])
        res = '\n'.join([res, 'NEWLIB_LIB?=$(TC_PATH)/$(OSNAME)_x86_newlib/bin/i686-nacl-ar r'])
        res = '\n'.join([res, 'NEWLIB_CCFLAGS?=-MMD -pthread $(NACL_WARNINGS) -idirafter $(NACL_SDK_ROOT)/include'])
        res = '\n'.join([res, 'NEWLIB_LDFLAGS?=-pthread'])
        res = '\n'.join([res, ''])
        res = '\n'.join([res, '# ARM compiler'])
        res = '\n'.join([res, 'ARM_CC?=$(TC_PATH)/$(OSNAME)_arm_newlib/bin/arm-nacl-gcc -c'])
        res = '\n'.join([res, 'ARM_CXX?=$(TC_PATH)/$(OSNAME)_arm_newlib/bin/arm-nacl-g++ -c'])
        res = '\n'.join([res, 'ARM_LINK?=$(TC_PATH)/$(OSNAME)_arm_newlib/bin/arm-nacl-g++ -Wl,-as-needed'])
        res = '\n'.join([res, 'ARM_LIB?=$(TC_PATH)/$(OSNAME)_arm_newlib/bin/arm-nacl-ar r'])
        return res

    def mfCompilerGLibC(self):
        res = '# GLIC compiler'
        res = '\n'.join([res, 'GLIBC_CC?=$(TC_PATH)/$(OSNAME)_x86_glibc/bin/i686-nacl-gcc -c'])
        res = '\n'.join([res, 'GLIBC_CXX?=$(TC_PATH)/$(OSNAME)_x86_glibc/bin/i686-nacl-g++ -c'])
        res = '\n'.join([res, 'GLIBC_LINK?=$(TC_PATH)/$(OSNAME)_x86_glibc/bin/i686-nacl-g++ -Wl,-as-needed'])
        res = '\n'.join([res, 'GLIBC_LIB?=$(TC_PATH)/$(OSNAME)_x86_glibc/bin/i686-nacl-ar r'])
        res = '\n'.join([res, 'GLIBC_DUMP?=$(TC_PATH)/$(OSNAME)_x86_glibc/x86_64-nacl/bin/objdump'])
        res = '\n'.join([res, 'GLIBC_PATHS:=-L $(TC_PATH)/$(OSNAME)_x86_glibc/x86_64-nacl/lib32'])
        res = '\n'.join([res, 'GLIBC_PATHS+=-L $(TC_PATH)/$(OSNAME)_x86_glibc/x86_64-nacl/lib'])
        res = '\n'.join([res, 'GLIBC_CCFLAGS?=-MMD -pthread $(NACL_WARNINGS) -idirafter $(NACL_SDK_ROOT)/include'])
        res = '\n'.join([res, 'GLIBC_LDFLAGS?=-pthread'])
        return res

    def mfCompilerPNaCl(self):
        res = '# PNACL compiler'
        res = '\n'.join([res, 'PNACL_CC?=$(TC_PATH)/$(OSNAME)_x86_pnacl/newlib/bin/pnacl-clang -c'])
        res = '\n'.join([res, 'PNACL_CXX?=$(TC_PATH)/$(OSNAME)_x86_pnacl/newlib/bin/pnacl-clang++ -c'])
        res = '\n'.join([res, 'PNACL_LINK?=$(TC_PATH)/$(OSNAME)_x86_pnacl/newlib/bin/pnacl-clang++'])
        res = '\n'.join([res, 'PNACL_LIB?=$(TC_PATH)/$(OSNAME)_x86_pnacl/newlib/bin/pnacl-ar r'])
        res = '\n'.join([res, 'PNACL_CCFLAGS?=-MMD -pthread $(NACL_WARNINGS) -idirafter $(NACL_SDK_ROOT)/include'])
        res = '\n'.join([res, 'PNACL_LDFLAGS?=-pthread'])
        res = '\n'.join([res, 'TRANSLATE:=$(TC_PATH)/$(OSNAME)_x86_pnacl/newlib/bin/pnacl-translate'])
        return res

    def mfCompiler(self):
        res = '# Compiler defines'
        if EToolChain.NEWLIB in self.__setting__.toolchains:
            res = '\n'.join([res, ''])
            res = '\n'.join([res, self.mfCompilerNewLib()])
        if EToolChain.GLIBC in self.__setting__.toolchains:
            res = '\n'.join([res, ''])
            res = '\n'.join([res, self.mfCompilerGLibC()])
        if EToolChain.PNACL in self.__setting__.toolchains:
            res = '\n'.join([res, ''])
            res = '\n'.join([res, self.mfCompilerPNaCl()])
        return res

    def mfFlag(self):
        res = '# Flag defines'
        if EMakeType.DEBUG in self.__setting__.maketypes:
            res = '\n'.join([res, 'CXXFLAGS_DEBUG:=-g -O0'])
        if EMakeType.PROFILE in self.__setting__.maketypes:
            res = '\n'.join([res, 'CXXFLAGS_PROFILE:=-g -2 -msse -mfpmath=sse -ffast-math -fomit-frame-pointer'])
        if EMakeType.DEPLOY in self.__setting__.maketypes:
            res = '\n'.join([res, 'CXXFLAGS_DEPLOY:=-s -3 -msse -mfpmath=sse -ffast-math -fomit-frame-pointer'])
        return res

    def makefile(self):
        res = self.mfDefine()
        res = '\n'.join([res, ''])
        res = '\n'.join([res, self.mfPath()])
        res = '\n'.join([res, ''])
        res = '\n'.join([res, self.mfCommand()])
        res = '\n'.join([res, ''])
        res = '\n'.join([res, self.mfDefault()])
        res = '\n'.join([res, ''])
        res = '\n'.join([res, self.mfCompiler()])
        res = '\n'.join([res, ''])
        res = '\n'.join([res, self.mfFlag()])
        return res