'''
Created on 2013-2-7

@author: Alex Chi
'''

def Enum(**enums):
    '''
From: StackOverflow
Url: http://stackoverflow.com/questions/36932/whats-the-best-way-to-implement-an-enum-in-python
    '''
    return type('Enum', (), enums)

EToolChain = Enum(NEWLIB = 'newlib', GLIBC = 'glibc', PNACL = 'pnacl')
EOSType = Enum(X86_32 = 'x86_32', X86_64 = 'x86_64', ARM = 'arm')
EMakeType = Enum(DEBUG = 'debug', PROFILE = 'profile', DEPLOY = 'deploy')
EProjectType = Enum(SLIB = 'a', DLIB = 'so', EXE = 'exe')
ECodeFileExt = Enum(C = 'c', CPP = 'cpp', CC = 'cc', H = 'h', HPP = 'hpp', HH = 'hh', INC = 'inc', INL = 'inl')
ECodeFileType = Enum(INC = 'inc', SRC = 'src', NONE='none')
ECodeType = Enum(C = 'c', CC = 'cc')

def UniqueSet(aset):
    '''
remove repeated item in set
    '''
    return set(aset)

def UniqueList(alist):
    '''
remove repeated item in list
    '''
    return list(UniqueSet(alist))

def ConvertExt(toolchain, projecttype):
    '''
can't convert the extension by EToolChain and EProjectType
    '''
    if projecttype is EProjectType.SLIB:
        return 'a'
    elif projecttype is EProjectType.DLIB:
        return 'so'
    elif projecttype is EProjectType.EXE:
        if toolchain is EToolChain.NEWLIB \
            or toolchain is EToolChain.GLIBC:
            return 'nexe'
        elif toolchain is EToolChain.PNACL:
            return 'pexe'
        else:
            raise Exception('error, can\'t find EToolChain.' + toolchain)
    else:
        raise Exception('error, can\'t find EProjectType.' + projecttype)

def SplitFullName(fullname):
    '''
split the full name to string array
    '''
    fnlist = fullname.split('.')
    name = ''
    ext = fnlist[len(fnlist) - 1]
    if 2 > len(fnlist):
        raise Exception('the format isn\'t available.')
    elif 2 <= len(fnlist):
        name = fnlist[0]
        for index in range(1, len(fnlist) - 1):
            name = '.'.join([name, fnlist[index]])
    return (name, ext)

def GetCodeFileType(ext):
    if ext in ['h', 'hh', 'hpp']:
        return ECodeFileType.INC
    if ext in ['c', 'cc', 'cpp']:
        return ECodeFileType.SRC
    return ECodeFileType.NONE