// Copyright (c) 2012 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// C headers
#include <cassert>
#include <cstdio>

// C++ headers
#include <sstream>
#include <string>

// NaCl
#include "ppapi/cpp/input_event.h"
#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/module.h"
#include "ppapi/cpp/point.h"
#include "ppapi/cpp/var.h"

namespace NaClAutomake{

    const char* const kDidChangeView = "DidChangeView";
    const char* const kDidChangeFocus = "DidChangeFocus";
    const char* const kHaveFocus = "HaveFocus";
    const char* const kDontHaveFocus = "DontHaveFocus";

    class DemoInstance : public pp::Instance
    {
    public:
        explicit DemoInstance(PP_Instance instance) : pp::Instance(instance) { ; }
        virtual ~DemoInstance() { ; }

        void DidChangeView(const pp::View& view)
        {
            PostMessage(pp::Var(kDidChangeView));
        }

        void DidChangeFocus(bool focus)
        {
            PostMessage(pp::Var(kDidChangeFocus));
            if (focus == true)
            {
                PostMessage(pp::Var(kHaveFocus));
            }
            else
            {
                PostMessage(pp::Var(kDontHaveFocus));
            }
        }
    };

    class DemoModule : public pp::Module
    {
    public:
        DemoModule() : pp::Module() {}
        virtual ~EventModule() {}

        virtual pp::Instance* CreateInstance(PP_Instance instance)
        {
            return new DemoInstance(instance);
        }
    };

}

namespace pp{

    Module* CreateModule()
    {
        return new NaClAutomake::DemoModule();
    }

}
