'''
Created on 2013-2-23

@author: Alex
'''
from naclautomake import CreateAutomake
from naclautomake.common import EToolChain
from naclautomake.common import EOSType
from naclautomake.common import EMakeType
from naclautomake.common import EProjectType

import unittest

class TestNaClAutomake(unittest.TestCase):
    def test(self):
        am = CreateAutomake('demo', 'solutionpath', 'outputpath', 'naclsdkpath', 'chromepath'
            , [EToolChain.NEWLIB, EToolChain.PNACL, EToolChain.GLIBC]
            , [EOSType.X86_32, EOSType.X86_64, EOSType.ARM]
            , [EMakeType.DEBUG, EMakeType.DEPLOY, EMakeType.PROFILE])
        demo = am.solution.newProject('demo', './', EProjectType.EXE)
        demo.addCodeFile('demo.cc', '')
        demo.addIncPath('incpath1')
        demo.addIncPath('incpath2')
        print am.makefile()
        am.write('Makefile')

if __name__ == '__main__':
    unittest.main()
